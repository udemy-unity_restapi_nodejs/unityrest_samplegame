﻿using System;
using System.Collections;
using UnityEngine;

namespace MyGame.Scripts
{
    public class Particle : MonoBehaviour, IParticle
    {
        private ParticleSystem _particleSystem;

        private void Awake()
        {
            _particleSystem = GetComponent<ParticleSystem>();
        }

        public IEnumerable Trigger()
        {
            _particleSystem.Play();
            yield return null;
        }
    }
}