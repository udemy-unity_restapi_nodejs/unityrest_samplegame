using System;
using System.Collections;
using System.Collections.Generic;
using Ez;
using MyGame.Scripts;
using UnityEngine;

public class Actions : MonoBehaviour 
{
    public enum ActionType
    {
        KILL,
        SCORE,
        LEVEL
    }

    [SerializeField] private ActionType actionType;

    private PlayerMovement _playerMovement;

    // private void Start()
    // {
    //     _playerMovement = GameObject.FindWithTag("Player").GetComponent<PlayerMovement>();
    // }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && other.TryGetComponent(out _playerMovement))
        {
            switch (actionType)
            {
                case ActionType.KILL:
                    _playerMovement.SendPlayerData(name);
                    break;
                case ActionType.SCORE:
                    _playerMovement.UpdateScore(2.5f);
                    break;
                case ActionType.LEVEL:
                    _playerMovement.UpgradeLevel();
                    break;
                default:
                    break;
            }
            gameObject.Send<IParticle>(_=>_.Trigger(), true);
            // EzMsg.Send<IArmor>(other.gameObject, _ => _.ApplyDamage(Damage))
            //     .Wait(2f)
            //     .Send<IWeapon>(gameObject, _=>_.Reload())
            //     .Run();

        }
        
    }
}
