using System;

[Serializable]
public class PlayerData
{
    public string name;
    public float score;
    public int level;
    public string killerName;
}
