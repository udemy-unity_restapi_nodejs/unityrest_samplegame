﻿using System.Collections;
using UnityEngine.EventSystems;

namespace MyGame.Scripts
{
    public interface IParticle : IEventSystemHandler
    {
        IEnumerable Trigger();
    }
}