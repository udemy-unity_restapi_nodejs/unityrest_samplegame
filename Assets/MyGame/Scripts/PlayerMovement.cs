using System;
using System.Collections;
using System.Collections.Generic;
using MyGame.Scripts;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private GameObject body;

    [Space(11)]
    [SerializeField] private PlayerData playerData;
    
    public static bool isMovementEnabled = false;
    
    private NavMeshAgent _navMeshAgent;

    private void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        ClickToMove();
    }

    void ClickToMove()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && isMovementEnabled)
        {
            if (Physics.Raycast(ray, out hit, 100))
            {
                _navMeshAgent.destination = hit.point;
            }
        }
    }

    public void SendPlayerData(string killerName)
    {
        playerData.name = PlayerPrefs.GetString(PlayerPrefConstants.PlayerName);
        playerData.killerName = killerName;
        
        //Send to server
        var json = JsonUtility.ToJson(playerData);
        Debug.Log("Player Data json: " + json);
    }

    public PlayerData PlayerData
    {
        get => playerData;
        private set => playerData = value;
    }

    public void UpdateScore(float value)
    {
        playerData.score += value;
        Debug.Log("Player Score: " + playerData.score);
    }

    public void UpgradeLevel()
    {
        playerData.level++;
        Debug.Log("Player Score: " + playerData.level);
    }

    public void UpdateKillerName(string name)
    {
        playerData.killerName = name;
        Debug.Log("Player name: " + playerData.killerName);
    }
}
