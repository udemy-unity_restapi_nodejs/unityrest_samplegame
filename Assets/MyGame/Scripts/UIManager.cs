using System.Collections;
using System.Collections.Generic;
using MyGame.Scripts;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
     [SerializeField] private TMP_InputField inputNamePlayer;
     [SerializeField] private GameObject canvasPanel;

     public void EnterButton()
     {
          if (inputNamePlayer.text.Length < 4)
          {
               Debug.LogWarning("Nome muito curto. Tente novamente xD");

               return;
          }
          
          PlayerPrefs.SetString(PlayerPrefConstants.PlayerName, inputNamePlayer.text);
          
          PlayerMovement.isMovementEnabled = true;
          canvasPanel.SetActive(false);
     }
}
